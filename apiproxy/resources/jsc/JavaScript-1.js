var correlationId = context.getVariable('request.header.X-Correlation-ID');
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("CorellationId", correlationId);
context.setVariable("timeStamp", timeStamp);
var billingId = context.getVariable('request.queryparam.billingId');
context.setVariable("invalidParam", "false");

var billigIdRegExp = /^[0-9]/;
var regex = new RegExp("^[0-9]{4,}$");

if (billingId === null ) {
    context.setVariable("invalidParam", "true");
    context.setVariable("errorMessage", "Please provide a valid billingId (numeric value and minimum length should be 4)");
    context.setVariable("queryParam", "billingId");
    
} else {
    var billingIds = billingId.split(",");

    (function validateBillingId() {
        
        for( var id=0; id<billingIds.length; id++) {
            
            if(!billigIdRegExp.test(billingIds[id])) {
                context.setVariable("errorMessage", "Please provide the numeric value for billingId.");
                context.setVariable("queryParam", "billingId");
                context.setVariable("invalidParam", "true");
                return "false";
                
            } else if( !regex.test(billingIds[id]) ) {
                context.setVariable("invalidParam", "true");
                context.setVariable("queryParam", "billingId");
                context.setVariable("errorMessage", "String is too short (" + billingIds[id].length + " chars), minimum 4");

            return "false";
            }
        }
    }())
}
